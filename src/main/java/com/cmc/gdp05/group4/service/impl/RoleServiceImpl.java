package com.cmc.gdp05.group4.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmc.gdp05.group4.entity.Role;
import com.cmc.gdp05.group4.repository.RolePermissionRepository;
import com.cmc.gdp05.group4.repository.RoleRepository;
import com.cmc.gdp05.group4.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService{
	@Autowired
	private RoleRepository roleRepository  ;
	@Override
	public List<Role> findAll() {
		return roleRepository.findAll();
	}

	@Override
	public boolean insert(Role role) {
		boolean isSuccess = false;
		Role role_query = roleRepository.save(role);
		if(role_query != null ) {
			isSuccess = true;
		}
		return isSuccess; 
	}

	@Override
	public boolean update(Role role) {
		boolean isSuccess = false;
		Role role_query = roleRepository.save(role);
		if(role_query != null ) {
			isSuccess = true;
		}
		return isSuccess;
	}

	@Override
	public boolean delete(int roleID) {
		boolean isSuccess = false;
		try {
			Role role = roleRepository.getOne(roleID);
			roleRepository.delete(role);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return isSuccess;
	}

}
