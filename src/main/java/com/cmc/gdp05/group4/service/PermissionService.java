package com.cmc.gdp05.group4.service;

import java.util.List;

import com.cmc.gdp05.group4.entity.Permission;

/**
 * @author haopham
 *
 */
public interface PermissionService {
	/**
	* Create by: haopham - CMC
	* Create date: Jan 13, 2019
	* Modifier: haopham
	* Modified date: Jan 13, 2019
	* Description: ....
	* Version 1.0
	* @return
	*/
	public List<Permission> getListAllPermission();

}
