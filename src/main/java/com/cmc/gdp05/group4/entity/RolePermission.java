package com.cmc.gdp05.group4.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author haopham
 *
 */
@Entity
@Table(name="role_permission")
public class RolePermission {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name ="permission_id")
	private int permissionID;
	
	@Column(name ="role_id")
	private int roleID;
	
	private boolean status;
	
	public RolePermission() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RolePermission(int permissionID, int roleID, boolean status) {
		super();
		this.permissionID = permissionID;
		this.roleID = roleID;
		this.status = status;
	}

	public int getPermissionID() {
		return permissionID;
	}

	public void setPermissionID(int permissionID) {
		this.permissionID = permissionID;
	}

	public int getRoleID() {
		return roleID;
	}

	public void setRoleID(int roleID) {
		this.roleID = roleID;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	
	
	
}
