package com.cmc.gdp05.group4.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {
	@RequestMapping(value = "/")
	public String index(Model model) {
		return "admin/layouts/index";
	}
	@RequestMapping(value = "/user/list")
	public String list(Model model) {
		return "admin/users/list";
	}
	
	@RequestMapping(value= "/user/create")
	public String create() {
		return "admin/users/add" ;
	}
	
	
}
