package com.cmc.gdp05.group4.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.ResponseBody;

import com.cmc.gdp05.group4.entity.Permission;
import com.cmc.gdp05.group4.entity.Role;
import com.cmc.gdp05.group4.service.impl.PermissionServiceImpl;
import com.cmc.gdp05.group4.service.impl.RoleServiceImpl;

@Controller
public class RoleController {
	
	@Autowired
	private PermissionServiceImpl permissionService ;
	
	@Autowired
	private RoleServiceImpl roleServiceImpl ;
	
	@GetMapping(value = "/role/list")
	public String list(Model model) {
		return "admin/roles/list";
	}
	
	@GetMapping(value= "/role/create")
	public String create(ModelMap modelMap) {
		
		List<Permission> permissions = permissionService.getListAllPermission();
		modelMap.addAttribute("permissions",permissions);
		modelMap.addAttribute("role",new Role()) ;
		return "admin/roles/add" ;
	}
	@PostMapping(value = "/role/create")
	@ResponseBody
	public String store(@ModelAttribute("role") Role role) {
		roleServiceImpl.insert(role);
		String out = "<p>";
		
		out += "</p>";
		return out ;
		//return "redirect:/role/list" ;
	}
}
